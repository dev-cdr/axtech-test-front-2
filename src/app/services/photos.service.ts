import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { IPhotos } from '../models/photos.model';
import { IEmail } from '../models/email.model';

// type EntityResponseType = HttpResponse<IPhotos>;
// type EntityArrayResponseType = HttpResponse<IPhotos[]>;

@Injectable({
  providedIn: 'root'
})
export class PhotosService {
  public resourceUrl = environment.PHOTOS_URL;
  public postUrl = environment.POST_URL;

  constructor(
    private httpClient: HttpClient
  ) { }

  create(req: any): Observable<any> {
    return this.httpClient.post<any>(this.postUrl, req, { observe: 'response' });
  }

  search(req?: any): Observable<any> {
    return this.httpClient.get(this.resourceUrl, { params: req });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.httpClient.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
