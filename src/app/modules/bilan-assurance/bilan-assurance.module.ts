import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';


import { BilanAssuranceRoutingModule } from './bilan-assurance-routing.module';
import { SharedModule } from '../shared/shared.module';

import { LayoutComponent } from './layout/layout.component';
import { HomeComponent } from './pages/home/home.component';
import { BilanAssuranceComponent } from './components/bilan-assurance/bilan-assurance.component';
import { BilanCompletComponent } from './components/bilan-complet/bilan-complet.component';
import { BilanMailComponent } from './components/bilan-mail/bilan-mail.component';

@NgModule({
  declarations: [
    LayoutComponent,
    HomeComponent,
    BilanAssuranceComponent,
    BilanCompletComponent,
    BilanMailComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    BilanAssuranceRoutingModule
  ],
  bootstrap: [LayoutComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class BilanAssuranceModule { }
