import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { environment } from '../../../../../environments/environment';
import { PhotosService } from '../../../../services/photos.service';
import { IEmail } from '../../../../models/email.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-bilan-mail',
  templateUrl: './bilan-mail.component.html',
  styleUrls: ['./bilan-mail.component.scss']
})
export class BilanMailComponent implements OnInit, OnDestroy {
  // Subscriptions
  private subscriptions: Array<Subscription> = [];

  // URL for the photos
  public resourceUrl = environment.PHOTOS_URL;

  // Managing the form
  public formGroupEmail: FormGroup;
  public email: FormControl;

  // Params for query photos
  public paramsEmail: Array<IEmail> = [];
  public paramsPost = {
    title: 'Test front',
    body: '<email_utilisateur>',
    userId: 1
  };

  constructor(
    public photosService: PhotosService
  ) { }

  ngOnInit() {
    // Managing form
    this.createFormControls();
    this.createForm();
  }

  /**
   * Creating formControls
   */
  createFormControls() {
    this.email = new FormControl('', [Validators.required]);
  }

  createForm() {
    this.formGroupEmail = new FormGroup({
        email: this.email,
    });
  }

  onSubmit() {
    this.paramsPost.body = this.formGroupEmail.value.email;

    this.subscriptions.push(
      this.photosService.create(this.paramsPost).subscribe(creaPhotos => {
        if (creaPhotos.status === 201) {
          this.formGroupEmail.reset();
        }
      }, error => {
        console.log('this.photosService ERROR : ', error);
      }),
    );
  }

  /**
   * Unsubscription
   */
  ngOnDestroy() {
    // Unsubscription
    this.subscriptions.forEach(element => {
      element.unsubscribe();
    });
  }
}
