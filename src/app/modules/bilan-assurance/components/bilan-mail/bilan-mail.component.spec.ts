import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BilanMailComponent } from './bilan-mail.component';

describe('BilanMailComponent', () => {
  let component: BilanMailComponent;
  let fixture: ComponentFixture<BilanMailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BilanMailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BilanMailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
