import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { PhotosService } from '../../../../services/photos.service';
import { Subscription } from 'rxjs';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { IPhotos } from '../../../../models/photos.model';

@Component({
  selector: 'app-bilan-assurance',
  templateUrl: './bilan-assurance.component.html',
  styleUrls: ['./bilan-assurance.component.scss']
})
export class BilanAssuranceComponent implements OnInit, OnDestroy {
  // Subscriptions
  private subscriptions: Array<Subscription> = [];

  // URL for the photos
  public resourceUrl = environment.PHOTOS_URL;

  // Params for query photos
  public paramsPhotos = {
    _start: 12,
    _limit: 5
  };

  public listPhotos: Array<IPhotos> = [];
  public nbStars: Array<number> = [];
  public arrModulo: Array<number> = [];

  constructor(
    public photosService: PhotosService
  ) { }

  ngOnInit() {
    this.subscriptions.push(
      this.photosService.search(this.paramsPhotos).subscribe(resPhotos => {
        this.listPhotos = resPhotos;
        this.nbStars = Array.from(Array(this.listPhotos.length).keys());

        this.listPhotos.map( (photo) => {
          this.arrModulo.push((photo.id % 5) + 1);
        });
      }, error => {
        console.log('this.photosService ERROR : ', error);
      }),
    );
  }

  /**
   * Unsubscription
   */
  ngOnDestroy() {
    this.subscriptions.forEach(element => {
      element.unsubscribe();
    });
  }

  deletePhoto(id: number, index: number) {
    this.subscriptions.push(
      this.photosService.delete(id).subscribe(delPhotos => {
        if (delPhotos.status === 200) {
          this.listPhotos.splice(index, 1);
        }

      }, error => {
        console.log('this.photosService ERROR : ', error);
      }),
    );
  }

}
