import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BilanAssuranceComponent } from './bilan-assurance.component';

describe('BilanAssuranceComponent', () => {
  let component: BilanAssuranceComponent;
  let fixture: ComponentFixture<BilanAssuranceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BilanAssuranceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BilanAssuranceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
