import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BilanCompletComponent } from './bilan-complet.component';

describe('BilanCompletComponent', () => {
  let component: BilanCompletComponent;
  let fixture: ComponentFixture<BilanCompletComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BilanCompletComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BilanCompletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
