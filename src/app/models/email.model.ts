export interface IEmail {
    title?: string;
    body?: string;
    userId?: number;
}

export class Email implements IEmail {
    constructor(
        public title?: string,
        public body?: string,
        public userId?: number
    ) { }
}
