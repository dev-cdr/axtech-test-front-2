export interface IPhotos {
    id?: number;
    albumId?: number;
    title?: string;
    url?: string;
    thumbnailUrl?: string;
}

export class Photos implements IPhotos {
    constructor(
        public id?: number,
        public albumId?: number,
        public title?: string,
        public url?: string,
        public thumbnailUrl?: string,
    ) {
    }
}
